# Domain Role Access
Provides alternative per-domain access control based on roles.

In addition to ordinary access control from the **domain_access** module
(through the field in user's profile), this module adds an ability to assign
roles to domain records.

Users, which have these roles will have the same access to this domain,
as if they had these domains filled in their profiles.

Please note, that this module does not provide any additional access restrictions or grants.
It only adds a way to control "domain access" user field on a per-role basis (instead of standard per-user).
The final access values are defined using "OR" logic: domain_access field + domain roles (i.e. it does not override anything, only adds new records).

