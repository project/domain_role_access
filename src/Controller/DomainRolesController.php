<?php

namespace Drupal\domain_role_access\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\domain_role_access\Form\DomainRolesForm;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DomainRolesController.
 *
 * @package Drupal\domain_role_access\Controller
 */
class DomainRolesController extends ControllerBase {

  /**
   * Edit domain roles.
   *
   * @return array
   *   Edit form page.
   */
  public function edit($domain) {
    /** @var DomainLoaderInterface $domainLoader */
    $domainLoader = \Drupal::service('domain.loader');
    if (!$domain = $domainLoader->load($domain)) {
      throw new NotFoundHttpException();
    }
    $build = [
      'edit_form' => $this->formBuilder()->getForm(DomainRolesForm::class, $domain),
    ];

    return $build;
  }

}
